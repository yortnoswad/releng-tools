#!/bin/bash

# Put a marker file in place so mirrormanager knows not to crawl when we're in the middle of a sync
ssh -i sshkey centos@master-1.centos.org "echo $(date +%Y%m%d-%H%M%S) > /home/centos-stream/9-stream/.sync_in_progress"

# We exclude isos so we can bring their links over
# We exclude COMPOSE_ID to make it the last things we sync
date; rsync -avhH -e "ssh -i sshkey" --copy-links --delay-updates --delete-after --progress --exclude=".sync_in_progress" --exclude="iso/" --exclude="COMPOSE_ID" /mnt/centos/staged/9-stream/ centos@master-1.centos.org:/home/centos-stream/9-stream/

# rsync over all the iso directories
date; for arch in aarch64 ppc64le s390x x86_64; do rsync -avhH -e "ssh -i sshkey" --delay-updates --delete-after --progress /mnt/centos/staged/9-stream/BaseOS/$arch/iso/ centos@master-1.centos.org:/home/centos-stream/9-stream/BaseOS/$arch/iso/ ; done

# sync COMPOSE_ID last because that is what mirrormanager looks for
date; rsync -avhH -e "ssh -i sshkey" --progress /mnt/centos/staged/9-stream/COMPOSE_ID centos@master-1.centos.org:/home/centos-stream/9-stream/COMPOSE_ID

# Remove marker file
ssh -i sshkey centos@master-1.centos.org "/usr/bin/rm -f /home/centos-stream/9-stream/.sync_in_progress"
