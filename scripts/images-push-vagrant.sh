#!/usr/bin/bash

# Check Input
OS_VERSION="${1}"
RELEASES=(9 10)
if ! [[ " ${RELEASES[*]} " =~ " ${OS_VERSION} " ]] ; then
    echo ""
    echo "ERROR: ${OS_VERSION} IS NOT a release"
    echo ""
    echo "Usage: ${0} release"
    echo ""
    echo "Valid values of release are: ${RELEASES[*]}"
    exit 1
fi

# Setup Global Variables
if [ "${OS_VERSION}" == "9" ] ; then
    COMPOSE_DIR="production"
else
    COMPOSE_DIR="stream-${OS_VERSION}/production"
fi
VERSION="$(cat /mnt/centos/staged/${OS_VERSION}-stream/COMPOSE_ID | cut -d'-' -f4 )"
PROVIDERS="libvirt virtualbox"
USER="centos"
BOX="stream${OS_VERSION}"
ARCH="x86_64"
CHECKSUM_TYPE="sha256"

# Ensure we have a token
if [[ -z ${hcp_client_id} ]] ; then
  echo "Please set [hcp_client_id] as an environment variable" >&2
  exit 1
elif [[ -z ${hcp_client_secret} ]] ; then
  echo "Please set [hcp_client_secret] as an environment variable" >&2
  exit 1
fi


# All based on https://developer.hashicorp.com/hcp/docs/hcp/api#authenticate-to-hcp
#  and https://developer.hashicorp.com/hcp/api-docs/vagrant-box-registry#overview
# First auth to hcp portal
curl --silent --fail --location "https://auth.idp.hashicorp.com/oauth2/token" \
  --header "Content-Type: application/x-www-form-urlencoded" \
  --data-urlencode "client_id=${hcp_client_id}" \
  --data-urlencode "client_secret=${hcp_client_secret}" \
  --data-urlencode "grant_type=client_credentials" \
  --data-urlencode "audience=https://api.hashicorp.cloud" > returned_json

if [ "$?" -ne "0" ] ; then
  echo "Unable to auth with Hashicorp. Exiting"
  exit 1
fi

hcp_access_token=$(jq .access_token returned_json | tr -d '"')
/bin/rm returned_json


# Create the version first
echo ; echo "== Creating ${BOX} box version: ${VERSION}" ; echo
curl --silent --fail --request POST \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer ${hcp_access_token}" \
  --data '{ "name" : "'"${VERSION}"'" }' \
  https://api.cloud.hashicorp.com/vagrant/2022-09-30/registry/centos/box/${BOX}/versions | jq


for PROVIDER in ${PROVIDERS}
do
  FILENAME="CentOS-Stream-Vagrant-${OS_VERSION}-${VERSION}.${ARCH}.vagrant-${PROVIDER}.box"
  BOX_URL="https://cloud.centos.org/centos/${OS_VERSION}-stream/${ARCH}/images/${FILENAME}"
  CHECKSUM_URL="${BOX_URL}.${CHECKSUM_TYPE^^}SUM"

  CHECKSUM="$(curl --silent "$CHECKSUM_URL" | awk -F ' = ' "/\($FILENAME\)/ { print \$2 }")"
  if [[ -z $CHECKSUM ]] ; then
    echo "error: ${FILENAME} not in ${CHECKSUM_URL}" >&2
    exit 1
  fi

  # Creating first provider
  echo ; echo "== Creating ${BOX} box provider: ${PROVIDER}" ; echo
  curl --silent --fail --request POST \
    --header "Content-Type: application/json" \
    --header "Authorization: Bearer ${hcp_access_token}" \
    --data '{ "name": "'"${PROVIDER}"'" }' \
    https://api.cloud.hashicorp.com/vagrant/2022-09-30/registry/centos/box/${BOX}/version/${VERSION}/providers | jq
 
  # Updating provider with box settings/architecture
  curl --silent --fail --request POST \
    --header "Content-Type: application/json" \
    --header "Authorization: Bearer ${hcp_access_token}" \
    --data '{ "architecture_type": "amd64", "default": true, "box_data": { "download_url": "'"${BOX_URL}"'", "checksum": "'"${CHECKSUM}"'", "checksum_type": "SHA256" } } ' \
    https://api.cloud.hashicorp.com/vagrant/2022-09-30/registry/centos/box/${BOX}/version/${VERSION}/provider/${PROVIDER}/architectures | jq


done

# Release the version last
echo ; echo "== Releasing ${BOX} box version: ${VERSION}" ; echo

curl --silent --fail --request PUT \
  --header "Content-Type: application/json" \
  --header "Authorization: Bearer ${hcp_access_token}" \
  https://api.cloud.hashicorp.com/vagrant/2022-09-30/registry/centos/box/${BOX}/version/${VERSION}/release | jq



