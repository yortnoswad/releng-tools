set -eux

rm -rf ./cccc
git clone https://pagure.io/fedora-ci/cccc.git
cd cccc

export CCCC_CONFIG_FILE="./cccc.conf"

# This will grab the stream number from the target branch name
if [[ "$gitlabTargetBranch" =~ centos-([0-9]+)-stream || "$gitlabTargetBranch" =~ c([0-9]+)s ]]; then
  streamNumber="${BASH_REMATCH[1]}"
else
  if [ "$gitlabTargetBranch" == "main" ]; then
    streamNumber="9"
  else
    echo "Branch name $gitlabTargetBranch does not match the expected pattern. The script expects the 'c10s' or 'centos-10-stream' branch format. If you see this error, please open a ticket in the CS project on issues.redhat.com and we'll have a look. Thanks."
    exit 1
  fi
fi

targetStream="c${streamNumber}s"
targetPungiBranch="centos-${streamNumber}-stream"

echo "targetStream is set to $targetStream"
echo "targetPungiBranch is set to $targetPungiBranch"
echo "merged_repo_url=\"https://distrobaker-bot:$TOKEN@gitlab.com/redhat/centos-stream/release-engineering/cccc-merged-configs.git\"" > ./cccc.conf
echo 'odcs_auth_method="kerberos"' >> cccc.conf
echo 'odcs_server_url="https://odcs.stream.rdu2.redhat.com/"' >> cccc.conf
echo "odcs_auth_file=\"$KEYTAB\"" >> cccc.conf
echo "merged_repo_id_file=\"$CCCC_SSH_KEY\"" >> cccc.conf
echo "odcs_raw_config_name=\"${targetStream}_cccc\"" >> cccc.conf
echo "merged_repo_pungi_config=\"cccc.conf\"" >> cccc.conf
echo "bugzilla_product=\"CentOS Stream $streamNumber\""
cat cccc.conf

env

# c9s and lower have modules, so we need to include the module defaults repo
# c10s and higher don't

if [[ "$targetStream" == "c8s" || "$targetStream" == "c9s" ]]; then

    if [[ "$gitlabSourceRepoHttpUrl" == *"pungi-centos"* ]]; then
        PYTHONPATH=. python3 cccc --pr-pungi-repo "$gitlabSourceRepoHttpUrl#proposed/$gitlabBranch" \
            --pr-mod-defaults-repo "https://gitlab.com/redhat/centos-stream/release-engineering/module-defaults.git#$targetStream" \
            --default-pungi-repo "https://gitlab.com/redhat/centos-stream/release-engineering/pungi-centos.git#$targetPungiBranch" \
            --default-pungi-file centos-development.conf --skip-phase osbs --skip-phase buildinstall --skip-phase image_build --skip-phase extra_isos --skip-phase createiso
    elif [[ "$gitlabSourceRepoHttpUrl" == *"comps"* ]]; then
        PYTHONPATH=. python3 cccc --pr-comps-repo "$gitlabSourceRepoHttpUrl#$gitlabBranch" \
            --pr-mod-defaults-repo "https://gitlab.com/redhat/centos-stream/release-engineering/module-defaults.git#$targetStream" \
            --default-pungi-repo "https://gitlab.com/redhat/centos-stream/release-engineering/pungi-centos.git#$targetPungiBranch" \
            --default-pungi-file centos-development.conf --skip-phase osbs --skip-phase buildinstall --skip-phase image_build --skip-phase extra_isos --skip-phase createiso
    fi

else

    if [[ "$gitlabSourceRepoHttpUrl" == *"pungi-centos"* ]]; then
        PYTHONPATH=. python3 cccc --pr-pungi-repo "$gitlabSourceRepoHttpUrl#proposed/$gitlabBranch" \
            --default-pungi-repo "https://gitlab.com/redhat/centos-stream/release-engineering/pungi-centos.git#$targetPungiBranch" \
            --default-pungi-file centos-development.conf --skip-phase osbs --skip-phase buildinstall --skip-phase image_build --skip-phase extra_isos --skip-phase createiso
    elif [[ "$gitlabSourceRepoHttpUrl" == *"comps"* ]]; then
        PYTHONPATH=. python3 cccc --pr-comps-repo "$gitlabSourceRepoHttpUrl#$gitlabBranch" \
            --default-pungi-repo "https://gitlab.com/redhat/centos-stream/release-engineering/pungi-centos.git#$targetPungiBranch" \
            --default-pungi-file centos-development.conf --skip-phase osbs --skip-phase buildinstall --skip-phase image_build --skip-phase extra_isos --skip-phase createiso
    fi

fi

exit $?

